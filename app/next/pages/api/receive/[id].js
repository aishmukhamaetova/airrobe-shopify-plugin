import Cors from 'cors'
import { fromUsers } from '../../../src/users'

function initMiddleware(middleware) {
  return (req, res) =>
    new Promise((resolve, reject) => {
      middleware(req, res, (result) => {
        if (result instanceof Error) {
          return reject(result)
        }
        return resolve(result)
      })
    })
}

const cors = initMiddleware(
  Cors({
    methods: ['GET', 'POST', 'OPTIONS'],
  })
)

export default async (req, res) => {
  await cors(req, res)

  const {
    query: { id },
  } = req

  fromUsers.add({ email: id })

  res.statusCode = 200
  res.json(fromUsers.list)
}