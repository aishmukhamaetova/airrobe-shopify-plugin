import React from 'react'
import Head from 'next/head'
import { fromUsers } from '../src/users'

function Order({ doc }) {
  const items = doc?.line_items || []

  return (
    <div>
      <h2>Order ({doc.name})</h2>
      <p>Customer email: {doc.email}</p>
      <h3>Items</h3>
      {items.map(({ name, price, variant_id }, key) => (
        <div key={key}>
          <h4>{name} ({variant_id}) - ${price}</h4>
        </div>
      ))}
    </div>

  )
}

export default function Home({ users, orders }) {
  console.log(`USERS`, users)
  console.log(`ORDERS`, orders)

  const list = orders?.orders || []

  return (
    <div className="container">
      <Head>
        <title>AirRobe Cloud</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <div>
          <h3>Recently tracked users</h3>
          {users.map(({ email }, key) => <div key={key}>{email}</div>)}
        </div>
        <div>
          <h3>Orders</h3>
          {list.map((doc, key) => <Order key={key} doc={doc} />)}
        </div>

      </main>

      <style jsx>{`
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  )
}

Home.getInitialProps = async (ctx) => {
  const res = await fetch('https://a37243737f598369e11b3e08dc005ec8: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@airrobeshop.myshopify.com/admin/api/2020-07/orders.json')
  const json = await res.json()

  return { orders: json, users: fromUsers.list }
}